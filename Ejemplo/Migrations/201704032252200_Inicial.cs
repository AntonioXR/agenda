namespace Ejemplo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categorias",
                c => new
                    {
                        CategoriaId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Contactoes",
                c => new
                    {
                        ContactoId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Numero = c.Int(nullable: false),
                        Direccion = c.String(nullable: false, maxLength: 100),
                        Correo = c.String(nullable: false, maxLength: 50),
                        CategoriaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ContactoId)
                .ForeignKey("dbo.Categorias", t => t.CategoriaId, cascadeDelete: true)
                .Index(t => t.CategoriaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contactoes", "CategoriaId", "dbo.Categorias");
            DropIndex("dbo.Contactoes", new[] { "CategoriaId" });
            DropTable("dbo.Contactoes");
            DropTable("dbo.Categorias");
        }
    }
}
