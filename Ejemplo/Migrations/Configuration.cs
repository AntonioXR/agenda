namespace Ejemplo.Migrations
{
    using Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Ejemplo.Models.WebApiEjemploContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        //Add.Migration Inicial
        //Update-database
        protected override void Seed(Ejemplo.Models.WebApiEjemploContext context)
        {

            context.Categoria.AddOrUpdate(
                new Categoria() {CategoriaId = 1,Nombre = "Amigos"}, 
                new Categoria() {CategoriaId = 2, Nombre = "Universidad"}, 
                new Categoria() {CategoriaId = 3, Nombre = "Trabajo"});

            context.Contacto.AddOrUpdate(
                new Contacto()
                {
                    ContactoId = 1,
                    Nombre = "Angel",
                    Numero = 12456589,
                    Direccion = "zona 2",
                    Correo = "angel@gmail.com",
                    CategoriaId = 1
                },
                new Contacto()
                {
                    ContactoId = 2,
                    Nombre = "Ros",
                    Numero = 12456589,
                    Direccion = "zona 1",
                    Correo = "ros@gmail.com",
                    CategoriaId = 2
                },
                new Contacto()
                {
                    ContactoId = 3,
                    Nombre = "Carlos",
                    Numero = 12456589,
                    Direccion = "zona 3",
                    Correo = "carlos@gmail.com",
                    CategoriaId = 3
                });
        }
    }
}
