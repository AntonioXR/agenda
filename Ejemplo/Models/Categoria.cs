﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ejemplo.Models
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        [Required, StringLength(20)]
        public String Nombre { get; set; }
        public Categoria()
        {

        }
        public Categoria(int id,String nom)
        {
            this.CategoriaId = id;
            this.Nombre = nom;
        }
    }
}