﻿namespace Ejemplo.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class WebApiEjemploContext : DbContext
    {
        public WebApiEjemploContext()
            : base("name=WebApiEjemploContext")
        {
            
        }
        //PM> Enable-Migrations
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<Contacto> Contacto { get; set; }
    }
}