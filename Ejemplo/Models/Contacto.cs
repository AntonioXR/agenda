﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ejemplo.Models
{
    public class Contacto
    {
        public int ContactoId { get; set; }
        [Required, StringLength(50)]
        public String Nombre { get; set; }
        [Required]
        public int Numero { get; set; }
        [Required, StringLength(100)]
        public String Direccion { get; set; }
        [Required, StringLength(50)]
        public String Correo { get; set; }
        [Required]
        public int CategoriaId { get; set; }
        public Categoria Categoria { get; set; }

        public Contacto()
        {
            this.ContactoId = 0;
            this.Nombre = String.Empty;
            this.Numero = 0;
            this.Direccion = String.Empty;
            this.Correo = String.Empty;
            this.CategoriaId = 0;
            this.Categoria = null;
        }
        public Contacto(int id, String nombre, int numero, String direccion, Categoria ca)
        {
            this.ContactoId = id;
            this.Nombre = nombre;
            this.Numero = numero;
            this.Direccion = direccion;
            this.Categoria = ca;
        }
    }
}