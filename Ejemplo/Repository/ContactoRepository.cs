﻿using Ejemplo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ejemplo.Repository
{
    public class ContactoRepository
    {
        #region "Atributos"
        private static ContactoRepository instancia = new ContactoRepository();
        private List<Contacto> listaContacto = new List<Contacto>();
        private int idContacto = 0;
        #endregion
        #region "Metodos"
        public Contacto Agregar(Contacto contacto)
        {
            listaContacto.Add(contacto);
            return contacto;
        }

        public Contacto Editar(Contacto contacto)
        {
            foreach (Contacto temp in listaContacto)
            {
                if (temp.ContactoId == contacto.ContactoId)
                {
                    listaContacto.Remove(temp);
                    listaContacto.Add(contacto);
                }
            }
            return contacto;
        }
        public Contacto Eliminar(int id)
        {
            Contacto contacto = listaContacto.Find(c => c.ContactoId == id);
            if (contacto == null)
            {
                return null;
            }
            listaContacto.Remove(contacto);
            return contacto;
        }
        public Contacto Buscar(int id)
        {
            return listaContacto.Find(c => c.ContactoId == id);
        }
        public IEnumerable<Contacto> Contactos()
        {
            return listaContacto;
        }
        public static ContactoRepository Instancia
        {
            get { return instancia; }
        }
        private int GetId()
        {
            return this.idContacto++;
        }
        #endregion
        public ContactoRepository()
        {
            this.listaContacto = new List<Contacto>()
            {
                new Contacto() {ContactoId = GetId(), Nombre = "Roberto", Numero = 20121235, Direccion = "zona 1", Categoria = CategoriaRepository.Instancia.Buscar(1) },
                new Contacto() {ContactoId = GetId(), Nombre = "Isabel", Numero = 52654878, Direccion = "zona 2", Categoria = CategoriaRepository.Instancia.Buscar(2)  },
                new Contacto() {ContactoId = GetId(), Nombre = "Marela", Numero = 45896532, Direccion = "zona 3", Categoria = CategoriaRepository.Instancia.Buscar(2)  }
            };
        }
    }
}