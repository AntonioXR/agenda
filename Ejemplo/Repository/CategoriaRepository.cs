﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ejemplo.Models;

namespace Ejemplo.Repository
{
    public class CategoriaRepository
    {
        #region "Atributos"
        private static CategoriaRepository instancia = new CategoriaRepository();
        private List<Categoria> listaCategoria = new List<Categoria>();
        private int idCategoria = 0;
        #endregion
        #region "Metodos"
        public Categoria Agregar(Categoria categoria)
        {
            listaCategoria.Add(categoria);
            return categoria;
        }

        public Categoria Editar(Categoria categoria)
        {
            foreach(Categoria temp in listaCategoria){
                if(temp.CategoriaId == categoria.CategoriaId)
                {
                    listaCategoria.Remove(temp);
                    listaCategoria.Add(categoria);
                }
            }
            return categoria;
        }
        public Categoria Eliminar(int id)
        {
            Categoria categoria = listaCategoria.Find(c => c.CategoriaId == id);
            if(categoria == null)
            {
                return null;
            }
            listaCategoria.Remove(categoria);
            return categoria;
        }
        public Categoria Buscar(int id)
        {
            return listaCategoria.Find(c => c.CategoriaId == id);
        }
        public IEnumerable<Categoria> Categorias()
        {
            return listaCategoria;
        }
        public static CategoriaRepository Instancia
        {
            get { return instancia; }
        }
        private int GetId()
        {
            return this.idCategoria++;
        }
        #endregion
        public CategoriaRepository()
        {
            this.listaCategoria = new List<Categoria>()
            {
                new Categoria() {CategoriaId = GetId(), Nombre = "Amigos" },
                new Categoria() {CategoriaId = GetId(), Nombre = "Familia" },
                new Categoria() {CategoriaId = GetId(), Nombre = "Trabajo" }
            };
        }
    }
}