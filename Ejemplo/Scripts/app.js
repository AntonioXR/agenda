﻿$(document).ready(function () {
    CargarTablaCategorias();
});

function CargarTablaCategorias() {
    var app = null;
    $.ajax({
        url: "../api/Categoria/",
        type: "get",
        dataType: "json",
        contentType: "application/json",
        success: function (response) {
            app = response;
            console.log(app);
        },
        error: function (response) {
            console.log("OCURRIO UN ERROR");
            console.log(response);
        }
    });
    var fila = "";
    $.each(app, function (index, item) {
        fila += "<tr><td>" + item.CategoriaId + "</td><td>" + item.Nombre + "</td><td><button class='btn btn-success'>Editar</button><button class='btn btn-warning'>Eliminar</button></td></tr>";
    });
    $("#table-categorias").append(fila);
}
function Reiniciar() {

}

function AgregarCategoria() {
    $.ajax({
        url: 'api/Categoria',
        type: 'post',
        dataType: "json",
        contentType: "application/json",
        data: {
            Nombre: $("inputNombre").val()
        }
    }).done(function (response) {
        alert(response);
    });
}
function FormularioAgregar() {
    $("#mostrarCategorias").slideToggle("slow", function () {
        $("#nueva-categoria").slideToggle("slow");
    });
}