﻿var ViewModel = function () {
    //Declaracion de Variables a usar
    var main = this;
    var CategoriaUri = 'api/Categoria/';
    var ContactoUri = 'api/Contacto/';
    main.categorias = ko.observableArray();
    main.contactos = ko.observableArray();
    main.error = ko.observable();
    //Variable a usar para el CRUD
    //Variable para Agregar
    main.newCategoria = {
        Nombre: ko.observable()
    }
    main.newContacto = {
        Nombre: ko.observable(),
        Numero: ko.observable(),
        Direccion: ko.observable(),
        Correo: ko.observable(),
        CategoriaId: 0,
        Categoria: ko.observable()
    }
    //AGREGAR
    main.agregar = function (formElement) {
        var categoriaAdd = {
            Nombre: main.newCategoria.Nombre
        }
        ajaxHelper(CategoriaUri, "post", categoriaAdd).done(function (response) {
            getAllCategorias();
            main.categorias.push(response);
        });
    }
    main.agregarContacto = function (formElement) {
        var contactoAdd = {
            Nombre: main.newContacto.Nombre,
            Numero: main.newContacto.Numero,
            Direccion: main.newContacto.Direccion,
            Correo: main.newContacto.Correo,
            CategoriaId: main.newContacto.Categoria.CategoriaId,
            Categoria: main.newContacto.Categoria
        }
        console.log(main.newContacto.Nombre);
        console.log(contactoAdd);
        ajaxHelper(ContactoUri, "post", contactoAdd).done(function (response){
            getAllContactos();
            main.contactos.push(response);
        });
    }
    //Procesos de Carga para el Formulario
    main.categoriaCargada = ko.observable();
    main.cargar = function (item) {
        main.categoriaCargada(item);
    }
    main.contactoCargado = ko.observable();
    main.cargarContacto = function (item) {
        main.contactoCargado(item);
    }
    //EDITAR
    main.editar = function (formElement) {
        var CategoriaEditar = {
            CategoriaId: main.categoriaCargada().CategoriaId,
            Nombre: main.categoriaCargada().Nombre
        }
        ajaxHelper(CategoriaUri + CategoriaEditar.CategoriaId, "put", CategoriaEditar).done(function (response) {
            getAllCategorias();
            main.categorias.push(response);
        });
    }
    main.editarContacto = function (formElement) {
        var CategoriaContacto = BuscarCategoria(main.newContacto.CategoriaId);
        var ContactoEditar = {
            ContactoId: main.contactoCargado.ContactoId,
            Nombre: main.contactoCargado.Nombre,
            Numero: main.contactoCargado.Numero,
            Direccion: main.contactoCargado.Direccion,
            Correo: main.contactoCargado.Correo,
            CategoriaId: main.contactoCargado.CategoriaId,
            Categoria: CategoriaContacto
        }
        ajaxHelper(ContactoUri + ContactoEditar.ContactoId, "put", ContactoEditar).done(function (response) {
            getAllContactos();
            main.contantactos.push(response);
        });
    }
    //ELIMINAR
    main.eliminar = function (item) {
        ajaxHelper(CategoriaUri + item.CategoriaId, "delete").done(function () {
            getAllCategorias();
        });
    }
    main.eliminarContacto = function (item) {
        ajaxHelper(ContactoUri + item.ContactoId, "delete").done(function () {
            getAllContactos();
        });
    }

//Proceso para cargar la tabla getAll{Tabla}
function getAllContactos(){
    ajaxHelper(ContactoUri, "GET").done(function (response) {
        main.contactos(response);
    });
}

function getAllCategorias() {
    ajaxHelper(CategoriaUri, "GET").done(function (data) {
        main.categorias(data);
    });
}

function BuscarCategoria(Id){
    var Categoria = null;
    for(var i = 0; i < main.categorias.length; i++){
        if(main.categorias[i].CategoriaId == Id){
            Categoria = main.categorias[i];
        }
    }
    return Categoria;
}
//Conexion a Web Services 
function ajaxHelper(uri, method, data) {
    main.error("");
    return $.ajax({
        url: uri,
        type: method,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data): null
    }).fail(function (jqXHR, status, error) {
        main.error(error);
        console.log("jqXHR: " + jqXHR);
        console.log("Status: "+status);
    })
}
getAllCategorias();
getAllContactos();
}

$(document).ready(function () {
    var viewMdel = new ViewModel();
    ko.applyBindings(viewMdel);
})