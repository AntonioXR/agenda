﻿using Ejemplo.Models;
using Ejemplo.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Ejemplo.Controllers
{
    public class CategoriaController : ApiController
    {
        private WebApiEjemploContext db = new WebApiEjemploContext();
        //Get api/Categoria
        public IEnumerable<Categoria> GetCategorias()
        {
            return db.Categoria;
        }

        //GET: api/Categoria/id
        [ResponseType(typeof(Categoria))]
        public async Task<IHttpActionResult> GetCategoria(int id)
        {
            Categoria cat = await db.Categoria.FindAsync(id);

            if (cat == null)
            {
                return NotFound();
            }

            return Ok(cat);
        }
        //GET: api/Categoria/order/tipo
        [Route("api/Categoria/order/{evaluacion}")]
        public IEnumerable<Categoria> GetCategoriasOrdenadas(String evaluacion)
        {
            List<Categoria> s = new List<Categoria>();
            if (evaluacion.Equals("Asc"))
            {
                return CategoriaRepository.Instancia.Categorias().OrderBy(c => c.CategoriaId);
            }
            else if (evaluacion.Equals("Desc"))
            {
                return CategoriaRepository.Instancia.Categorias().OrderByDescending(c => c.CategoriaId);
            }
            return s;
        }

        //PUT: api/Categoria
        public async Task<IHttpActionResult> PutCategoria(int id, Categoria categoria)
        {
            if (!ModelState.IsValid || id != categoria.CategoriaId)
            {
                return BadRequest(ModelState);
            }
            db.Entry(categoria).State = EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi", new { id = categoria.CategoriaId }, categoria);
        }

        //DELETE: api/Categoria/id
        public async Task<IHttpActionResult> DeleteCategoria(int id)
        {
            Categoria categoria = await db.Categoria.FindAsync(id);
            if (categoria == null)
            {
                return NotFound();
            }
            db.Categoria.Remove(categoria);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = categoria.CategoriaId }, categoria);
        }

        [ResponseType(typeof(Categoria))]
        public async Task<IHttpActionResult> PostCategoria(Categoria categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Categoria.Add(categoria);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = categoria.CategoriaId }, categoria);
        }
    }
}
