﻿using Ejemplo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Ejemplo.Repository;

namespace Ejemplo.Controllers
{
    public class ContactoController : ApiController
    {
        private WebApiEjemploContext db = new WebApiEjemploContext();
        //Get api/Categoria
        public IEnumerable<Contacto> GetCategorias()
        {
            return db.Contacto.Include(c => c.Categoria);
        }

        //GET: api/Categoria/id
        [ResponseType(typeof(Contacto))]
        public async Task<IHttpActionResult> GetContacto(int id)
        {
            Contacto cat = await db.Contacto.Include(c => c.Categoria).SingleOrDefaultAsync(c => c.ContactoId == id);

            if (cat == null)
            {
                return NotFound();
            }

            return Ok(cat);
        }
        //GET: api/Categoria/order/tipo
        [Route("api/Contacto/order/Nombre/{evaluacion}")]
        public IEnumerable<Contacto> GetCategoriasOrdenadas(String evaluacion)
        {
            List<Contacto> s = new List<Contacto>();
            if (evaluacion.Equals("Asc"))
            {
                return ContactoRepository.Instancia.Contactos().OrderBy(c => c.Nombre);
            }
            else if (evaluacion.Equals("Desc"))
            {
                return ContactoRepository.Instancia.Contactos().OrderByDescending(c => c.Nombre);
            }
            return s;
        }

        //PUT: api/Categoria
        public async Task<IHttpActionResult> PutContacto(int id, Contacto contacto)
        {
            if (!ModelState.IsValid || id != contacto.CategoriaId)
            {
                return BadRequest(ModelState);
            }
            Contacto edit = ContactoRepository.Instancia.Editar(contacto);
            return CreatedAtRoute("DefaultApi", new { id = edit.CategoriaId }, edit);
        }

        //DELETE: api/Categoria/id
        public async Task<IHttpActionResult> DeleteContacto(int id)
        {
            Contacto eliminanada = ContactoRepository.Instancia.Eliminar(id);
            return CreatedAtRoute("DefaultApi", new { id = eliminanada.CategoriaId }, eliminanada);
        }

        [ResponseType(typeof(Contacto))]
        public async Task<IHttpActionResult> PostCategoria(Contacto contacto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Contacto nuevaCategoria = ContactoRepository.Instancia.Agregar(contacto);

            return CreatedAtRoute("DefaultApi", new { id = nuevaCategoria.CategoriaId }, nuevaCategoria);
        }
    }
}
